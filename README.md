# Portfolio
<br><br>
<img src="https://media.licdn.com/dms/image/D5603AQFyI0XUZFhJLw/profile-displayphoto-shrink_200_200/0/1677764683851?e=1683158400&v=beta&t=_5ehngvbY4YOx_ebiIjCCKLa-iOw2r6UP_mw9u3XVkU" alt="">
<br><br>
### Hello and welcome to my Portfolio. My name is Ekrem Tümen, and I am a passionate full stack developer with a focus on Java, Kotlin, and web technologies. In this file, you will find some of my projects that were created within the scope of my education.
<br><br>

## Projects 
 
### Reverse Tower Defender [Do You Know Da Wae](https://gitlab.com/ekrem.tuemen99/projekt-doyouknowdawae).

During my second semester of training at Codersbay, I completed a retro-style game called _Reverse Tower Defender: Do You Know Da Wae_. The focus of the project was on algorithmic design and implementation. This project demonstrates my skills in game development, problem-solving, and algorithm design.

***
### PHP Laravel Project [Warehouse management Platform](https://gitlab.com/ekrem.tuemen99/lagerverwaltung).

This project is a warehouse management system developed using Laravel. The goal of the project is to create a simple inventory management system for a warehouse. The system includes basic functionalities such as managing products and users, setting minimum stock levels, and allowing administrators and buyers to update stock levels.
Overall, this Project aims to provide a functional and user-friendly system for inventory management, with the potential for additional features to enhance its capabilities.

***
### Spring CRUD Project [Administration Platform](https://gitlab.com/Daniel.A.92/codersbayadministration).

This project is an administration platform that was developed using the Spring framework. The goal of the platform is to provide a tool for organizing trainers and students in a learning environment. With this platform, administrators can easily add and edit trainer and student information, as well as create groups and schedules.

The platform includes a range of features, including a user-friendly interface for managing trainer and student data. This includes adding and editing information such as names, email addresses, and phone numbers. In addition, administrators can create groups and assign trainers and students to them, allowing for easy management of classes.

***
### Mobile Application [Dungeons & Dragons Charakter Tracker](https://gitlab.com/ekrem.tuemen99/charTracker-kotlin-project).

A character tracker for the table-top game Dungeons & Dragons that provides an overview of the current session. It is divided into a Start, Home, Prepared Spells & Buffs, and Item View.

The idea of the app is to expand the sheets and make the game easier for the player. By taking on small tasks such as remembering whether one is concentrating on an ability or resetting all values after a rest, the player should be supported. Please
note that the app is still in development.

***
